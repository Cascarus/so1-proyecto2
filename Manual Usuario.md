# Manual de Usuario

# Sistemas Operativos 1

Proyecto 2 - Manual de Usuario

## Grupo 21

| Carnet | Integrante |
| :----: | :----: |
| 201700328 | Marcos Alberto Santos Aquino |
| 201314632 | Edgar Orlando Guamuch Zarate |
| 201612204 | Marylin Irenia Hernández Muñoz |
| 201503878 | Erick Fernando Sanchez Mejia |

## Objetivos

```bash
    - Realizar un sistema computacional distribuído, cloud native, utilizando diferentes servicios de mensajería,
contenedores y controladores de tráfico para ser aplicado a una problemática actual gestionado por kubernetes.
    - Generar tráfico y dividirlo en la red utilizando herramientas específicas como Locust y Google Cloud
Load Balancer.
    - Crear una app web utilizando React, uno de los frameworks con más demanda laboral en el mercado.
```

## Descripcion

```bash
    Dada la situación actual, con la pandemia COVID-19, se require hacer un análisis en tiempo real de los datos de
vacunandos alrededor de Guatemala y del mundo. Por ello, es necesario realizar un sistema que pueda almacenar los datos
de infecciones y mostrar gráficas relevantes; así tomar mejor decisiones en la búsqueda de métodos para
sobrellevar la contingencia de la mejor manera.
El sistema contará con una carga masiva de datos, los cuales tendrán un formato específico detallado más
adelante; además, contarán con una app web que mostrará las gráficas y métricas más relevantes de los datos
que se suministren al sistema.
Se utilizarán tres alternativas de middlewares de mensajería; cada uno de ellos será utilizado para enviar el
tráfico generado en conjunto, esto con el fin de tener una respuesta más rápida al momento de cargar datos,
además de utilizar tecnología de vanguardia para sobrellevar este sistema. 
```


## Archivo de entrada

En Locust en el [archivo de entrada](./Locust/traffic.json) hay un array con elementos con la siguiente estructura:

```json
{
"name": "Marcos Santos",
"location": "Belgica",
"gender": "male",
"age": 18,
"vaccine_type": "Sputnik V"
}
```

## Especificaciones generales para los servidores

- Puerto: 7000
- Endpoint POST: <http://iphostvm/7000/>
- En servidor se agregara por que servidor ha pasado, ejemplo `"way":"gRPC"` quedando:

```json
{

    "Way": "Redis",
    "age": 18,
    "gender": "male",
    "location": "Belgica",
    "name": "Marcos Santos",
    "vaccine_type": "Sputnik V"

}
```
### LANDING PAGE

    - La pagina principal muestra un mapa del mundo, el cual permite la interacción al posiccionar el cursos sobre la region del pais que desee observar.
![image](https://drive.google.com/uc?export=view&id=1Kk_kZA8ZN5DBEudS9FffDDYaVuckzI8j)

    - Dentro de la misma landing page, es posible algunas opciones de usuario, la cual puede hacer un full       screen o bien aplicar nuestro minimalista diseño oscuro.
   ![image](https://drive.google.com/uc?export=view&id=1hiDpgFf57dx6vleh0I4unu0hKGmlBIPM)
   
### Menu Desplegable

    - La acción de la pagina es poder proveer el facil acceso y utilizar el maximo de pantalla que las mismas posean, por ello cuenta con un menú, donde puede navegar de manera agil.
    
    - Al presionar en el aréa de redis , es apreciable cada unao de los reportes los cuales arrojan ciertos analisis de datos, que permiten obtener conclusiones de una manera eficiente.
![image](https://drive.google.com/uc?export=view&id=1iUVwXUikyX20y58LK8INDtcXBcEXGuK_)


### Area de reportes
    - Ejemplos de nuestra area de reportes
    
![image](https://drive.google.com/uc?export=view&id=1ZdVIza3JENffWwLD6vh3QEs-fnwhDIrM)

![image](https://drive.google.com/uc?export=view&id=1lwyz7b5XhI80TDm14Mipy6DiuTriA_AG)

    - Informe de toda la data almacenada
![image](https://drive.google.com/uc?export=view&id=1PvbDpFEQcbz04lgiqYFYtn3wcU5RL8C6)

** muchas gracias **
