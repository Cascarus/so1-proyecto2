const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  age: {
    type: String,
    required: true
  },
  vaccine_type: {
    type: String,
    required: false
  },
  way: {
    type: String,
    required: true
  }
});

module.exports = Item = mongoose.model('item', ItemSchema);
