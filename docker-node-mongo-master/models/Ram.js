const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RamSchema = new Schema({
    memoria_total: {
      type: String,
      required: true
    },
    memoria_uso: {
      type: String,
      required: true
    },
    memoria_libre: {
      type: String,
      required: true
    },
    cached: {
      type: String,
      required: true
    },
    buffers: {
      type: String,
      required: true
    },
    porcentaje_de_memoria_uso: {
      type: String,
      required: true
    }
  });

  module.exports = Ram = mongoose.model('ram', RamSchema);