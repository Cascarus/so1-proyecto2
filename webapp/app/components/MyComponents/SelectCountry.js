import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { emphasize, makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import { lambda } from './../../services/lambda';
import axios from 'axios';

const suggestions = [
    { label: 'Afghanistan', value: 'Afghanistan'},
    { label: 'Aland Islands', value: 'Aland Islands'},
    { label: 'Albania', value: 'Albania'},
].map(suggestion => ({
    value: suggestion.label,
    label: suggestion.label,
}));

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        height: 110,
    },
    input: {
        display: 'flex',
        padding: 0,
        height: 'auto',
    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden',
    },
    chip: {
        margin: theme.spacing(0.5, 0.25),
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
            0.08,
        ),
    },
    noOptionsMessage: {
        padding: theme.spacing(1, 2),
    },
    singleValue: {
        fontSize: 16,
    },
    placeholder: {
        position: 'absolute',
        left: 8,
        bottom: 6,
        fontSize: 16,
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    },
}));

function NoOptionsMessage(props) {
    const { selectProps, innerProps, children } = props;
    return (
        <Typography
            color="textSecondary"
            className={selectProps.classes.noOptionsMessage}
            {...innerProps}
        >
            {children}
        </Typography>
    );
}

NoOptionsMessage.propTypes = {
    children: PropTypes.node,
    innerProps: PropTypes.object,
    selectProps: PropTypes.object.isRequired,
};

NoOptionsMessage.defaultProps = {
    children: null,
    innerProps: null
};

function inputComponent({ inputRef, ...props }) {
    return <div ref={inputRef} {...props} />;
}

inputComponent.propTypes = {
    inputRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
};

inputComponent.defaultProps = {
    inputRef: undefined
};

function Control(props) {
    const {
        children,
        innerProps,
        innerRef,
        selectProps: { classes, TextFieldProps },
    } = props;

    return (
        <TextField
            fullWidth
            InputProps={{
                inputComponent,
                inputProps: {
                    className: classes.input,
                    ref: innerRef,
                    children,
                    ...innerProps,
                },
            }}
            {...TextFieldProps}
        />
    );
}

Control.propTypes = {
    children: PropTypes.node,
    innerProps: PropTypes.object,
    innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
    selectProps: PropTypes.object.isRequired,
};

Control.defaultProps = {
    children: null,
    innerProps: null,
    innerRef: undefined
};

function Option(props) {
    const {
        innerRef,
        isFocused,
        isSelected,
        innerProps,
        children
    } = props;
    return (
        <MenuItem
            ref={innerRef}
            selected={isFocused}
            component="div"
            style={{
                fontWeight: isSelected ? 500 : 400,
            }}
            {...innerProps}
        >
            {children}
        </MenuItem>
    );
}

Option.propTypes = {
    children: PropTypes.node,
    innerProps: PropTypes.object,
    innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
    isFocused: PropTypes.bool,
    isSelected: PropTypes.bool,
};

Option.defaultProps = {
    children: null,
    innerProps: null,
    innerRef: undefined,
    isFocused: false,
    isSelected: false
};

function Placeholder(props) {
    const { selectProps, innerProps, children } = props;
    return (
        <Typography
            color="textSecondary"
            className={selectProps.classes.placeholder}
            {...innerProps}
        >
            {children}
        </Typography>
    );
}

Placeholder.propTypes = {
    children: PropTypes.node,
    innerProps: PropTypes.object,
    selectProps: PropTypes.object.isRequired,
};

Placeholder.defaultProps = {
    children: null,
    innerProps: null,
};

function SingleValue(props) {
    const { selectProps, children, innerProps } = props;
    return (
        <Typography className={selectProps.classes.singleValue} {...innerProps}>
            {children}
        </Typography>
    );
}

SingleValue.propTypes = {
    children: PropTypes.node,
    innerProps: PropTypes.object,
    selectProps: PropTypes.object.isRequired,
};

SingleValue.defaultProps = {
    children: null,
    innerProps: null,
};

function ValueContainer(props) {
    const { selectProps, children } = props;
    return <div className={selectProps.classes.valueContainer}>{children}</div>;
}

ValueContainer.propTypes = {
    children: PropTypes.node,
    selectProps: PropTypes.object.isRequired,
};

ValueContainer.defaultProps = {
    children: null,
};

function MultiValue(props) {
    const {
        children,
        selectProps,
        removeProps,
        isFocused
    } = props;
    return (
        <Chip
            tabIndex={-1}
            label={children}
            className={classNames(selectProps.classes.chip, {
                [selectProps.classes.chipFocused]: isFocused,
            })}
            onDelete={removeProps.onClick}
            deleteIcon={<CancelIcon {...removeProps} />}
        />
    );
}

MultiValue.propTypes = {
    children: PropTypes.node,
    isFocused: PropTypes.bool,
    removeProps: PropTypes.object.isRequired,
    selectProps: PropTypes.object.isRequired,
};

MultiValue.defaultProps = {
    children: null,
    isFocused: false,
};

function Menu(props) {
    const { selectProps, innerProps, children } = props;
    return (
        <Paper square className={selectProps.classes.paper} {...innerProps}>
            {children}
        </Paper>
    );
}

Menu.propTypes = {
    children: PropTypes.node,
    innerProps: PropTypes.object,
    selectProps: PropTypes.object,
};

Menu.defaultProps = {
    children: null,
    innerProps: null,
    selectProps: null,
};

const components = {
    Control,
    Menu,
    MultiValue,
    NoOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer,
};

const url = lambda + "/rango";

export default function SelectSuggestions() {
    const [msg, setMsg] = useState([]);

    const getMsg = async () => {
        /*try {
            const { data } = await axios.get(url);

            //console.log(data);
            //setMsg(data.map(Item => ({ text: Item.msg, name: Item.name, hour: Item.timeStamp })))
        }
        catch (err) {
            console.log(err);
        }*/

        /*
        const suggestions = [
            { label: 'Afghanistan', value: 'Afghanistan'},
            { label: 'Aland Islands', value: 'Aland Islands'},
            { label: 'Albania', value: 'Albania'},
        ];
        */

        axios.get(url).
            then(data => {
                const result = [];
                var contador = 0;
                data.data.forEach(item => {
                    //console.log(item);
                    //const key = item.name.charAt(0);
                    /*if (!result[key]) {
                        result[key] = 0
                    }*/
                    result[contador] = {pais: item.pais};
                    contador++;
                })

                //console.log(result);
                const newPuntos = Object.keys(result).map(k => ({
                    label: result[k].pais, value: result[k].pais
                }))
                //console.log(newPuntos);
                setMsg(newPuntos);
                
            })
    };

    //console.log(msg);

    useEffect(() => {
        const timeOut = setInterval(() => {
            getMsg();
        }, 1000)
        getMsg();
        return () => clearInterval(timeOut);
    }, [])

    const classes = useStyles();
    const theme = useTheme();
    const [multi, setMulti] = React.useState(null);

    function handleChangeMulti(value) {
        setMulti(value);
    }

    if(multi!= null){
        var concatenacionPaises = "";
        for(var i = 0; i < multi.length; i++){
            concatenacionPaises += multi[i].label+",";
            //console.log(multi[i].label);
        }
        if(localStorage.getItem('paises')){
            localStorage.removeItem('paises');
            concatenacionPaises = concatenacionPaises.substring(0, concatenacionPaises.length - 1);
            localStorage.setItem('paises', concatenacionPaises);
        }else{
            localStorage.setItem('paises', concatenacionPaises);
        }
        
    }else{
        localStorage.removeItem('paises');
    }
    

    const selectStyles = {
        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
    };

    return (
        <div className={classes.root}>
            <NoSsr>
                <Select
                    classes={classes}
                    styles={selectStyles}
                    inputId="react-select-multiple"
                    TextFieldProps={{
                        label: 'Paises',
                        InputLabelProps: {
                            htmlFor: 'react-select-multiple',
                            shrink: true,
                        },
                        placeholder: 'Select multiple countries',
                    }}
                    options={msg}
                    components={components}
                    value={multi}
                    onChange={handleChangeMulti}
                    isMulti
                />
            </NoSsr>
        </div>
    );
}