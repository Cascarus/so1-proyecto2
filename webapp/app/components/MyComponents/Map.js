import React, { Component, useEffect, useState } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldHigh";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import './am4chartMap.css';
import { Apiurl } from './../../services/apirest';
//import { FetchDataMap } from './FetchDataMap';
import axios from 'axios';

// Button imports
import Button from '@material-ui/core/Button';
import HdrWeak from '@material-ui/icons/HdrWeak'

var contador = 0;

/*const styles = theme => ({
    button: {
        margin: theme.spacing(1),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
});*/

class Am4chartMap extends Component {
    intervalID = null;
    async componentDidMount() {
        /* Chart code */
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        let chart = am4core.create("chartdiv", am4maps.MapChart);

        try {
            chart.geodata = am4geodata_worldLow;
        }
        catch (e) {
            chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
        }

        chart.projection = new am4maps.projections.Mercator();

        // zoomout on background click
        chart.chartContainer.background.events.on("hit", function () { zoomOut() });

        // you can have pacific - centered map if you set this to -154.8
        //chart.deltaLongitude = -10;

        let colorSet = new am4core.ColorSet();
        let morphedPolygon;

        // map polygon series (countries)
        let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
        polygonSeries.useGeodata = true;
        polygonSeries.calculateVisualCenter = true;
        // specify which countries to include
        //polygonSeries.include = ["IT", "CH", "FR", "DE", "GB", "ES", "PT", "IE", "NL", "LU", "BE", "AT", "DK"]
        //polygonSeries.include = ["ENG"];
        polygonSeries.exclude = ["AQ"];

        // country area look and behavior
        let polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.strokeOpacity = 1;
        polygonTemplate.stroke = am4core.color("#ffffff");
        polygonTemplate.fillOpacity = 0.5;
        polygonTemplate.tooltipText = "{name}";

        // desaturate filter for countries
        let desaturateFilter = new am4core.DesaturateFilter();
        desaturateFilter.saturation = 0.25;
        polygonTemplate.filters.push(desaturateFilter);

        // take a color from color set
        polygonTemplate.adapter.add("fill", function (fill, target) {
            return colorSet.getIndex(target.dataItem.index + 1);
        })



        // set fillOpacity to 1 when hovered
        let hoverState = polygonTemplate.states.create("hover");
        hoverState.properties.fillOpacity = 1;

        //URL API
        let url = Apiurl + "/mapa";


        // Pie chart
        let pieChart = chart.seriesContainer.createChild(am4charts.PieChart);
        // Set width/heigh of a pie chart for easier positioning only
        pieChart.width = 100;
        pieChart.height = 100;
        pieChart.hidden = true; // can't use visible = false!

        // because defauls are 50, and it's not good with small countries
        pieChart.chartContainer.minHeight = 1;
        pieChart.chartContainer.minWidth = 1;

        let pieSeries = pieChart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "category";

        // what to do when country is clicked
        polygonTemplate.events.on("hit", function (event) {
            //console.log(event.target.dataItem._dataContext.id);
            axios.get(url).then(data => {
                var siExiste = 0;
                for (var i = 0; i < data.data.length; i++) {
                    var alterado = cambiarPais(data.data[i]);
                    if (alterado._id == event.target.dataItem._dataContext.id) {
                        localStorage.setItem("pais", alterado._id);
                        var contadorMasculino = 0;
                        var contadorFemenino = 0;
                        for (var j = 0; j < alterado.departamento.length; j++) {
                            //console.log(alterado.departamento[j]);
                            if (alterado.departamento[j].genero == "female") {
                                contadorFemenino += alterado.departamento[j].contador;
                            } else {
                                contadorMasculino += alterado.departamento[j].contador;
                            }
                        }

                        var dataPie = [{ value: contadorMasculino, category: "Male" }, { value: contadorFemenino, category: "Female" }];
                        pieSeries.data = dataPie;

                        siExiste = 1;
                        break;
                    }
                }
                if (siExiste == 1) {
                    event.target.zIndex = 1000000;
                    //vis();
                    selectPolygon(event.target);
                } else {
                    siExiste = 0;
                }
            });
        })



        // Bubble series
        let bubbleSeries = chart.series.push(new am4maps.MapImageSeries());
        var data_ = [];
        var refresh = [];
        this.intervalID = setInterval(async () => {
            await axios.get(url).then(data => {
                //console.log(data.data[0] == undefined);
                if (data.data[0] == undefined) {
                    data_.length = 0;
                }
                data.data.forEach(item => {
                    var alterado = cambiarPais(item);
                    if (data_.length > 0) {
                        var encontrado = 0;
                        for (var i = 0; i < data_.length; i++) {
                            if (data_[i]._id == alterado._id) {
                                data_[i]._id = alterado._id;
                                data_[i].departamento = alterado.departamento;
                                data_[i].confirmed = alterado.confirmed;
                                data_[i].name = alterado.name;
                                encontrado = 1;
                                //////////////////////////
                                if (localStorage.getItem("pais") == alterado._id) {
                                    //console.log("--------------------------");
                                    var contadorMasculino = 0;
                                    var contadorFemenino = 0;
                                    for (var j = 0; j < alterado.departamento.length; j++) {

                                        //console.log(alterado.departamento[j]);
                                        if (alterado.departamento[j].genero == "female") {
                                            contadorFemenino += alterado.departamento[j].contador;
                                        } else {
                                            contadorMasculino += alterado.departamento[j].contador;
                                        }
                                    }

                                    var refresBool = 0;
                                    var posicion = 0;
                                    if (refresh.length > 0) {
                                        for (var p = 0; p < refresh.length; p++) {
                                            if (refresh[p].pais == alterado._id) {
                                                posicion = p;
                                                if (refresh[p].masculino == contadorMasculino && refresh[p].femenino == contadorFemenino) {
                                                    refresBool = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (refresBool == 0) {
                                        refresh.splice(posicion, 1);
                                        refresh.push({ pais: alterado._id, masculino: contadorMasculino, femenino: contadorFemenino });
                                        var dataPie = [{ value: contadorMasculino, category: "Male" }, { value: contadorFemenino, category: "Female" }];
                                        pieSeries.data = dataPie;
                                        //console.log(refresh);
                                    }
                                    //var dataPie = [{ value: contadorMasculino, category: "Male" }, { value: contadorFemenino, category: "Female" }];
                                    //pieSeries.data = dataPie;

                                }
                                //////////////////////////
                                break;
                            }
                        }
                        if (encontrado == 0) {
                            if (alterado) {

                            }
                            data_.push(alterado);
                        }
                    } else {
                        data_.push(alterado);
                    }
                    bubbleSeries.data = data_;
                })
            });
        }, 1500);


        let dropShadowFilter = new am4core.DropShadowFilter();
        dropShadowFilter.blur = 4;
        pieSeries.filters.push(dropShadowFilter);

        let sliceTemplate = pieSeries.slices.template;
        sliceTemplate.fillOpacity = 1;
        sliceTemplate.strokeOpacity = 0;

        let activeState = sliceTemplate.states.getKey("active");
        activeState.properties.shiftRadius = 0; // no need to pull on click, as country circle under the pie won't make it good

        let sliceHoverState = sliceTemplate.states.getKey("hover");
        sliceHoverState.properties.shiftRadius = 0; // no need to pull on click, as country circle under the pie won't make it good

        // we don't need default pie chart animation, so change defaults
        let hiddenState = pieSeries.hiddenState;
        hiddenState.properties.startAngle = pieSeries.startAngle;
        hiddenState.properties.endAngle = pieSeries.endAngle;
        hiddenState.properties.opacity = 0;
        hiddenState.properties.visible = false;

        // series labels
        let labelTemplate = pieSeries.labels.template;
        labelTemplate.nonScaling = true;
        labelTemplate.fill = am4core.color("#FFFFFF");
        labelTemplate.fontSize = 10;
        labelTemplate.background = new am4core.RoundedRectangle();
        labelTemplate.background.fillOpacity = 0.9;
        labelTemplate.padding(4, 9, 4, 9);
        labelTemplate.background.fill = am4core.color("#7678a0");

        // we need pie series to hide faster to avoid strange pause after country is clicked
        pieSeries.hiddenState.transitionDuration = 200;

        // country label
        let countryLabel = chart.chartContainer.createChild(am4core.Label);
        countryLabel.text = "Seleccione un país";
        countryLabel.fill = am4core.color("#7678a0");
        countryLabel.fontSize = 40;

        countryLabel.hiddenState.properties.dy = 1000;
        countryLabel.defaultState.properties.dy = 0;
        countryLabel.valign = "middle";
        countryLabel.align = "right";
        countryLabel.paddingRight = 50;
        countryLabel.hide(0);
        countryLabel.show();

        // select polygon
        function selectPolygon(polygon) {
            if (morphedPolygon != polygon) {
                let animation = pieSeries.hide();
                if (animation) {
                    vis();
                    animation.events.on("animationended", function () {
                        morphToCircle(polygon);
                    })
                }
                else {
                    morphToCircle(polygon);
                }
            }
        }

        // fade out all countries except selected
        function fadeOut(exceptPolygon) {
            for (var i = 0; i < polygonSeries.mapPolygons.length; i++) {
                let polygon = polygonSeries.mapPolygons.getIndex(i);
                if (polygon != exceptPolygon) {
                    polygon.defaultState.properties.fillOpacity = 0.5;
                    polygon.animate([{ property: "fillOpacity", to: 0.5 }, { property: "strokeOpacity", to: 1 }], polygon.polygon.morpher.morphDuration);
                }
            }
        }

        function zoomOut() {
            if (morphedPolygon) {
                pieSeries.hide();
                morphBack();
                fadeOut();
                countryLabel.hide();
                morphedPolygon = undefined;
            }
        }

        function morphBack() {
            if (morphedPolygon) {
                morphedPolygon.polygon.morpher.morphBack();
                let dsf = morphedPolygon.filters.getIndex(0);
                dsf.animate({ property: "saturation", to: 0.25 }, morphedPolygon.polygon.morpher.morphDuration);
            }
        }

        function vis() {
            var x = document.getElementById("Desintegrer_");
            if (x.style.display == "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        var otro = null;
        function handleClose() {
            zoomOut();
            vis();
            if (morphedPolygon) {
                morphedPolygon.polygon.morpher.morphBack();
                let dsf = morphedPolygon.filters.getIndex(0);
                dsf.animate({ property: "saturation", to: 0.25 }, morphedPolygon.polygon.morpher.morphDuration);
            }
            //chart.zoomToMapObject(otro, 0, true);
        }


        document.getElementById("Desintegrer_").addEventListener("click", handleClose);


        function morphToCircle(polygon) {
            vis();
            let animationDuration = polygon.polygon.morpher.morphDuration;
            // if there is a country already morphed to circle, morph it back
            morphBack();
            // morph polygon to circle
            polygon.toFront();
            polygon.polygon.morpher.morphToSingle = true;
            let morphAnimation = polygon.polygon.morpher.morphToCircle();

            polygon.strokeOpacity = 0; // hide stroke for lines not to cross countries

            polygon.defaultState.properties.fillOpacity = 1;
            polygon.animate({ property: "fillOpacity", to: 1 }, animationDuration);

            // animate desaturate filter
            let filter = polygon.filters.getIndex(0);
            filter.animate({ property: "saturation", to: 1 }, animationDuration);

            // save currently morphed polygon
            morphedPolygon = polygon;

            // fade out all other
            fadeOut(polygon);

            // hide country label
            countryLabel.hide();

            if (morphAnimation) {
                morphAnimation.events.on("animationended", function () {
                    zoomToCountry(polygon);
                })
            }
            else {
                zoomToCountry(polygon);
            }
        }
        
        function zoomToCountry(polygon) {
            otro = polygon;
            let zoomAnimation = chart.zoomToMapObject(polygon, 2.2, true);
            if (zoomAnimation) {
                zoomAnimation.events.on("animationended", function () {
                    showPieChart(polygon);
                })
            }
            else {
                showPieChart(polygon);
            }
        }

        function showPieChart(polygon) {
            polygon.polygon.measure();
            let radius = polygon.polygon.measuredWidth / 2 * polygon.globalScale / chart.seriesContainer.scale;
            pieChart.width = radius * 2;
            pieChart.height = radius * 2;
            pieChart.radius = radius;

            let centerPoint = am4core.utils.spritePointToSvg(polygon.polygon.centerPoint, polygon.polygon);
            centerPoint = am4core.utils.svgPointToSprite(centerPoint, chart.seriesContainer);

            pieChart.x = centerPoint.x - radius;
            pieChart.y = centerPoint.y - radius;

            let fill = polygon.fill;
            let desaturated = fill.saturate(0.3);

            for (var i = 0; i < pieSeries.dataItems.length; i++) {
                let dataItem = pieSeries.dataItems.getIndex(i);
                //dataItem.value = Math.round(Math.random() * 100);
                dataItem.slice.fill = am4core.color(am4core.colors.interpolate(
                    fill.rgb,
                    am4core.color("#ffffff").rgb,
                    0.2 * i
                ));

                dataItem.label.background.fill = desaturated;
                dataItem.tick.stroke = fill;
            }

            pieSeries.show();
            pieChart.show();

            countryLabel.text = "{name}";
            countryLabel.dataItem = polygon.dataItem;
            countryLabel.fill = desaturated;
            countryLabel.show();
        }


        /* Burbujas */


        //data.push({ "confirmed": 0 + contador++, "_id": "US", "name": "United Satates" });
        //bubbleSeries.data = data;
        //db.items.aggregate([{ $group:{  _id:"$location",  confirmed:{$sum:1}, departamento:{$push:{contador:{$sum:1},genero:"$gender"}} } }] )
        //{ "_id" : "Guatemala City", "vacunados" : 1, "departamento" : [ { "contador" : 1, "genero" : "male" } ] }
        //var data = [{ "confirmed": 20, "id": "US", "name": "United Satates" },{ "confirmed": 10, "id": "GT", "name": "Guatemala" }];
        //{ longitude: -89.4411, latitude: 14.6790, size: 6, tooltip: 0 + contador++, fill: "#3A9A50", name: "Chiquimula" }


        //bubbleSeries.dataFields.value = 8;
        bubbleSeries.dataFields.value = "confirmed";
        bubbleSeries.dataFields.id = "_id";

        // adjust tooltip
        bubbleSeries.tooltip.animationDuration = 0;
        bubbleSeries.tooltip.showInViewport = false;
        bubbleSeries.tooltip.background.fillOpacity = 0.2;
        bubbleSeries.tooltip.getStrokeFromObject = true;
        bubbleSeries.tooltip.getFillFromObject = false;
        bubbleSeries.tooltip.background.fillOpacity = 0.2;
        bubbleSeries.tooltip.background.fill = am4core.color("#000000");

        var imageTemplate = bubbleSeries.mapImages.template;

        // if you want bubbles to become bigger when zoomed, set this to false
        imageTemplate.nonScaling = true;
        imageTemplate.strokeOpacity = 0;
        imageTemplate.fillOpacity = 1;
        imageTemplate.tooltipText = "{name}: [bold]{value} vaccinated[/]";
        imageTemplate.applyOnClones = true;

        //imageTemplate.events.on("over", handleImageOver);
        //imageTemplate.events.on("out", handleImageOut);
        //imageTemplate.events.on("hit", handleImageHit);

        // this is needed for the tooltip to point to the top of the circle instead of the middle
        imageTemplate.adapter.add("tooltipY", function (tooltipY, target) {
            return -target.children.getIndex(0).radius;
        })

        // When hovered, circles become non-opaque  
        let imageHoverState = imageTemplate.states.create("hover");
        imageHoverState.properties.fillOpacity = 1;

        // add circle inside the image
        let circle = imageTemplate.createChild(am4core.Circle);
        // this makes the circle to pulsate a bit when showing it
        circle.fill = "green";
        circle.hiddenState.properties.scale = 0.0001;
        circle.hiddenState.transitionDuration = 2000;
        circle.defaultState.transitionDuration = 2000;
        circle.defaultState.transitionEasing = am4core.ease.elasticOut;
        // later we set fill color on template (when changing what type of data the map should show) and all the clones get the color because of this
        circle.applyOnClones = true;
        //circle.tooltipHTML = `<center><strong>HOLA</strong></center>`;

        // heat rule makes the bubbles to be of a different width. Adjust min/max for smaller/bigger radius of a bubble
        bubbleSeries.heatRules.push({
            "target": circle,
            "property": "radius",
            "min": 3,
            "max": 15,
            "dataField": "value"
        })

        // when data items validated, hide 0 value bubbles (because min size is set)
        bubbleSeries.events.on("dataitemsvalidated", function () {
            bubbleSeries.dataItems.each((dataItem) => {
                let mapImage = dataItem.mapImage;
                let circle = mapImage.children.getIndex(0);
                if (mapImage.dataItem.value == 0) {
                    circle.hide(0);
                }
                else if (circle.isHidden || circle.isHiding) {
                    circle.show();
                }
            })
        })

        imageTemplate.adapter.add("latitude", function (latitude, target) {
            var polygon = polygonSeries.getPolygonById(target.dataItem.id);
            if (polygon) {
                target.disabled = false;
                return polygon.visualLatitude;
            }
            else {
                target.disabled = true;
            }
            return latitude;
        })

        imageTemplate.adapter.add("longitude", function (longitude, target) {
            var polygon = polygonSeries.getPolygonById(target.dataItem.id);
            if (polygon) {
                target.disabled = false;
                return polygon.visualLongitude;
            }
            else {
                target.disabled = true;
            }
            return longitude;
        })

        function handleImageOver(event) {
            rollOverCountry(polygonSeries.getPolygonById(event.target.dataItem.id));
        }

        function handleImageOut(event) {
            rollOutCountry(polygonSeries.getPolygonById(event.target.dataItem.id));
        }

        function handleImageHit(event) {
            selectCountry(polygonSeries.getPolygonById(event.target.dataItem.id));
        }
        // what happens when a country is rolled-over
        function rollOverCountry(mapPolygon) {

            resetHover();
            if (mapPolygon) {
                mapPolygon.isHover = true;

                // make bubble hovered too
                var image = bubbleSeries.getImageById(mapPolygon.dataItem.id);
                if (image) {
                    image.dataItem.dataContext.name = mapPolygon.dataItem.dataContext.name;
                    image.isHover = true;
                }
            }
        }
        // what happens when a country is rolled-out
        function rollOutCountry(mapPolygon) {
            var image = bubbleSeries.getImageById(mapPolygon.dataItem.id)

            resetHover();
            if (image) {
                image.isHover = false;
            }
        }
        function resetHover() {
            polygonSeries.mapPolygons.each(function (polygon) {
                polygon.isHover = false;
            })

            bubbleSeries.mapImages.each(function (image) {
                image.isHover = false;
            })
        }
        /* Fin Burbujas */

        function cambiarPais(item) {
            if (item._id == "Andorra") {
                item.name = item._id;
                item._id = "AD";
            }
            if (item._id == "Emiratos Árabes Unidos") {
                item.name = item._id;
                item._id = "AE";
            }
            if (item._id == "Afganistán") {
                item.name = item._id;
                item._id = "AF";
            }
            if (item._id == "Antigua y Barbuda") {
                item.name = item._id;
                item._id = "AG";
            }
            if (item._id == "Anguilla") {
                item.name = item._id;
                item._id = "AI";
            }
            if (item._id == "Albania") {
                item.name = item._id;
                item._id = "AL";
            }
            if (item._id == "Armenia") {
                item.name = item._id;
                item._id = "AM";
            }
            if (item._id == "Antillas Holandesas") {
                item.name = item._id;
                item._id = "AN";
            }
            if (item._id == "Angola") {
                item.name = item._id;
                item._id = "AO";
            }
            if (item._id == "Argentina") {
                item.name = item._id;
                item._id = "AR";
            }
            if (item._id == "Samoa Americana") {
                item.name = item._id;
                item._id = "AS";
            }
            if (item._id == "Austria") {
                item.name = item._id;
                item._id = "AT";
            }
            if (item._id == "Australia") {
                item.name = item._id;
                item._id = "AU";
            }
            if (item._id == "Aruba") {
                item.name = item._id;
                item._id = "AW";
            }
            if (item._id == "Azerbayán") {
                item.name = item._id;
                item._id = "AZ";
            }
            if (item._id == "Bosnia-Herzegovina") {
                item.name = item._id;
                item._id = "BA";
            }
            if (item._id == "Barbados") {
                item.name = item._id;
                item._id = "BB";
            }
            if (item._id == "Bangladesh") {
                item.name = item._id;
                item._id = "BD";
            }
            if (item._id == "Bélgica") {
                item.name = item._id;
                item._id = "BE";
            }
            if (item._id == "Burkina Faso") {
                item.name = item._id;
                item._id = "BF";
            }
            if (item._id == "Bulgaria") {
                item.name = item._id;
                item._id = "BG";
            }
            if (item._id == "Bahrain") {
                item.name = item._id;
                item._id = "BH";
            }
            if (item._id == "Burundi") {
                item.name = item._id;
                item._id = "BI";
            }
            if (item._id == "Benín") {
                item.name = item._id;
                item._id = "BJ";
            }
            if (item._id == "Islas Bermudas") {
                item.name = item._id;
                item._id = "BM";
            }
            if (item._id == "Brunei Darussalam") {
                item.name = item._id;
                item._id = "BN";
            }
            if (item._id == "Bolivia") {
                item.name = item._id;
                item._id = "BO";
            }
            if (item._id == "Brazil") {
                item.name = item._id;
                item._id = "BR";
            }
            if (item._id == "Bahamas") {
                item.name = item._id;
                item._id = "BT";
            }
            if (item._id == "Bután") {
                item.name = item._id;
                item._id = "BS";
            }
            if (item._id == "Islas Buvet") {
                item.name = item._id;
                item._id = "BV";
            }
            if (item._id == "Botswana") {
                item.name = item._id;
                item._id = "BW";
            }
            if (item._id == "Bielorrusia") {
                item.name = item._id;
                item._id = "BY";
            }
            if (item._id == "Belice") {
                item.name = item._id;
                item._id = "BZ";
            }
            if (item._id == "Canada") {
                item.name = item._id;
                item._id = "CA";
            }
            if (item._id == "Isla de Cocos") {
                item.name = item._id;
                item._id = "CC";
            }
            if (item._id == "República Democrática del Congo") {
                item.name = item._id;
                item._id = "CD";
            }
            if (item._id == "República Centroafricana") {
                item.name = item._id;
                item._id = "CF";
            }
            if (item._id == "República del Congo") {
                item.name = item._id;
                item._id = "CG";
            }
            if (item._id == "Suiza") {
                item.name = item._id;
                item._id = "CH";
            }
            if (item._id == "Costa de marfil") {
                item.name = item._id;
                item._id = "CI";
            }
            if (item._id == "Islas Cook") {
                item.name = item._id;
                item._id = "CK";
            }
            if (item._id == "Chile") {
                item.name = item._id;
                item._id = "CL";
            }
            if (item._id == "Camerún") {
                item.name = item._id;
                item._id = "CM";
            }
            if (item._id == "China") {
                item.name = item._id;
                item._id = "CN";
            }
            if (item._id == "Colombia") {
                item.name = item._id;
                item._id = "CO";
            }
            if (item._id == "Costa Rica") {
                item.name = item._id;
                item._id = "CR";
            }
            if (item._id == "Checoslovaquia (antiguo país)") {
                item.name = item._id;
                item._id = "CS";
            }
            if (item._id == "Cuba") {
                item.name = item._id;
                item._id = "CU";
            }
            if (item._id == "Cabo Verde") {
                item.name = item._id;
                item._id = "CV";
            }
            if (item._id == "Islas Christmas") {
                item.name = item._id;
                item._id = "CX";
            }
            if (item._id == "Chipre") {
                item.name = item._id;
                item._id = "CY";
            }
            if (item._id == "República Checa") {
                item.name = item._id;
                item._id = "CZ";
            }
            if (item._id == "Alemania") {
                item.name = item._id;
                item._id = "DE";
            }
            if (item._id == "Djibouti") {
                item.name = item._id;
                item._id = "DJ";
            }
            if (item._id == "Dinamarca") {
                item.name = item._id;
                item._id = "DK";
            }
            if (item._id == "Dominica") {
                item.name = item._id;
                item._id = "DM";
            }
            if (item._id == "República Dominicana") {
                item.name = item._id;
                item._id = "DO";
            }
            if (item._id == "Argelia") {
                item.name = item._id;
                item._id = "DZ";
            }
            if (item._id == "Ecuador") {
                item.name = item._id;
                item._id = "EC";
            }
            if (item._id == "Estonia") {
                item.name = item._id;
                item._id = "EE";
            }
            if (item._id == "Egipto") {
                item.name = item._id;
                item._id = "EG";
            }
            if (item._id == "Sáhara Occidental") {
                item.name = item._id;
                item._id = "EH";
            }
            if (item._id == "Eritrea") {
                item.name = item._id;
                item._id = "ER";
            }
            if (item._id == "España") {
                item.name = item._id;
                item._id = "ES";
            }
            if (item._id == "Etiopía") {
                item.name = item._id;
                item._id = "ET";
            }
            if (item._id == "Finlandia") {
                item.name = item._id;
                item._id = "FI";
            }
            if (item._id == "Fiji") {
                item.name = item._id;
                item._id = "FJ";
            }
            if (item._id == "Islas Malvinas") {
                item.name = item._id;
                item._id = "FK";
            }
            if (item._id == "Micronesia") {
                item.name = item._id;
                item._id = "FM";
            }
            if (item._id == "Islas Feroe") {
                item.name = item._id;
                item._id = "FO";
            }
            if (item._id == "Francia") {
                item.name = item._id;
                item._id = "FR";
            }
            if (item._id == "Gabón") {
                item.name = item._id;
                item._id = "GA";
            }
            if (item._id == "Granada") {
                item.name = item._id;
                item._id = "GD";
            }
            if (item._id == "Georgia") {
                item.name = item._id;
                item._id = "GE";
            }
            if (item._id == "Guyana Francesa") {
                item.name = item._id;
                item._id = "GF";
            }
            if (item._id == "Guernsey") {
                item.name = item._id;
                item._id = "GG";
            }
            if (item._id == "Ghana") {
                item.name = item._id;
                item._id = "GH";
            }
            if (item._id == "Gibraltar") {
                item.name = item._id;
                item._id = "GI";
            }
            if (item._id == "Groenlandia") {
                item.name = item._id;
                item._id = "GL";
            }
            if (item._id == "Gambia") {
                item.name = item._id;
                item._id = "GM";
            }
            if (item._id == "Guinea") {
                item.name = item._id;
                item._id = "GN";
            }
            if (item._id == "Guadalupe") {
                item.name = item._id;
                item._id = "GP";
            }
            if (item._id == "Guinea Ecuatorial") {
                item.name = item._id;
                item._id = "GQ";
            }
            if (item._id == "Grecia") {
                item.name = item._id;
                item._id = "GR";
            }
            if (item._id == "Islas Georgias y Sandwich del Sur") {
                item.name = item._id;
                item._id = "GS";
            }
            if (item._id == "Guatemala") {
                item.name = item._id;
                item._id = "GT";
            }
            if (item._id == "Guam") {
                item.name = item._id;
                item._id = "GU";
            }
            if (item._id == "Guinea-Bissau") {
                item.name = item._id;
                item._id = "GW";
            }
            if (item._id == "Guayana") {
                item.name = item._id;
                item._id = "GY";
            }
            if (item._id == "Hong Kong") {
                item.name = item._id;
                item._id = "HK";
            }
            if (item._id == "Islas Heard y McDonald") {
                item.name = item._id;
                item._id = "HM";
            }
            if (item._id == "Honduras") {
                item.name = item._id;
                item._id = "HN";
            }
            if (item._id == "Croacia") {
                item.name = item._id;
                item._id = "HR";
            }
            if (item._id == "Haití") {
                item.name = item._id;
                item._id = "HT";
            }
            if (item._id == "Hungría") {
                item.name = item._id;
                item._id = "HU";
            }
            if (item._id == "Indonesia") {
                item.name = item._id;
                item._id = "ID";
            }
            if (item._id == "Irlanda") {
                item.name = item._id;
                item._id = "IE";
            }
            if (item._id == "Israel") {
                item.name = item._id;
                item._id = "IL";
            }
            if (item._id == "Isla de Man") {
                item.name = item._id;
                item._id = "IM";
            }
            if (item._id == "India") {
                item.name = item._id;
                item._id = "IN";
            }
            if (item._id == "Territorio británico del Océano Índico") {
                item.name = item._id;
                item._id = "IO";
            }
            if (item._id == "Iraq") {
                item.name = item._id;
                item._id = "IQ";
            }
            if (item._id == "Irán") {
                item.name = item._id;
                item._id = "IR";
            }
            if (item._id == "Islandia") {
                item.name = item._id;
                item._id = "IS";
            }
            if (item._id == "Italia") {
                item.name = item._id;
                item._id = "IT";
            }
            if (item._id == "Jamaica") {
                item.name = item._id;
                item._id = "JM";
            }
            if (item._id == "Jersey") {
                item.name = item._id;
                item._id = "JE";
            }
            if (item._id == "Jordania") {
                item.name = item._id;
                item._id = "JO";
            }
            if (item._id == "Japón") {
                item.name = item._id;
                item._id = "JP";
            }
            if (item._id == "Kenia") {
                item.name = item._id;
                item._id = "KE";
            }
            if (item._id == "Kyrgystán") {
                item.name = item._id;
                item._id = "KG";
            }
            if (item._id == "Camboya") {
                item.name = item._id;
                item._id = "KH";
            }
            if (item._id == "Kiribati") {
                item.name = item._id;
                item._id = "KI";
            }
            if (item._id == "Islas Comores") {
                item.name = item._id;
                item._id = "KM";
            }
            if (item._id == "San Kitts y Nevis") {
                item.name = item._id;
                item._id = "KN";
            }
            if (item._id == "Corea del Norte") {
                item.name = item._id;
                item._id = "KP";
            }
            if (item._id == "Corea del Sur") {
                item.name = item._id;
                item._id = "KR";
            }
            if (item._id == "Kuwait") {
                item.name = item._id;
                item._id = "KW";
            }
            if (item._id == "Islas Caimán") {
                item.name = item._id;
                item._id = "KY";
            }
            if (item._id == "Kazajistán") {
                item.name = item._id;
                item._id = "KZ";
            }
            if (item._id == "Laos") {
                item.name = item._id;
                item._id = "LA";
            }
            if (item._id == "Líbano") {
                item.name = item._id;
                item._id = "LB";
            }
            if (item._id == "Santa Lucía") {
                item.name = item._id;
                item._id = "LC";
            }
            if (item._id == "Liechtenstein") {
                item.name = item._id;
                item._id = "LI";
            }
            if (item._id == "Sri Lanka") {
                item.name = item._id;
                item._id = "LK";
            }
            if (item._id == "Liberia") {
                item.name = item._id;
                item._id = "LR";
            }
            if (item._id == "Lesoto") {
                item.name = item._id;
                item._id = "LS";
            }
            if (item._id == "Lituania") {
                item.name = item._id;
                item._id = "LT";
            }
            if (item._id == "Luxemburgo") {
                item.name = item._id;
                item._id = "LU";
            }
            if (item._id == "Letonia") {
                item.name = item._id;
                item._id = "LV";
            }
            if (item._id == "Libia") {
                item.name = item._id;
                item._id = "LY";
            }
            if (item._id == "Marruecos") {
                item.name = item._id;
                item._id = "MA";
            }
            if (item._id == "Mónaco") {
                item.name = item._id;
                item._id = "MC";
            }
            if (item._id == "Moldavia") {
                item.name = item._id;
                item._id = "MD";
            }
            if (item._id == "Madagascar") {
                item.name = item._id;
                item._id = "MG";
            }
            if (item._id == "Macedonia") {
                item.name = item._id;
                item._id = "MK";
            }
            if (item._id == "Islas Marshall") {
                item.name = item._id;
                item._id = "MH";
            }
            if (item._id == "Mali") {
                item.name = item._id;
                item._id = "ML";
            }
            if (item._id == "Birmania") {
                item.name = item._id;
                item._id = "MM";
            }
            if (item._id == "Mongolia") {
                item.name = item._id;
                item._id = "MN";
            }
            if (item._id == "Macao") {
                item.name = item._id;
                item._id = "MO";
            }
            if (item._id == "Islas Marianas") {
                item.name = item._id;
                item._id = "MP";
            }
            if (item._id == "Martinica") {
                item.name = item._id;
                item._id = "MQ";
            }
            if (item._id == "Mauritania") {
                item.name = item._id;
                item._id = "MR";
            }
            if (item._id == "Montserrat") {
                item.name = item._id;
                item._id = "MS";
            }
            if (item._id == "Malta") {
                item.name = item._id;
                item._id = "MT";
            }
            if (item._id == "Mauricio") {
                item.name = item._id;
                item._id = "MU";
            }
            if (item._id == "Maldivas") {
                item.name = item._id;
                item._id = "MV";
            }
            if (item._id == "Malawi") {
                item.name = item._id;
                item._id = "MW";
            }
            if (item._id == "México") {
                item.name = item._id;
                item._id = "MX";
            }
            if (item._id == "Malasia") {
                item.name = item._id;
                item._id = "MY";
            }
            if (item._id == "Mozambique") {
                item.name = item._id;
                item._id = "MZ";
            }
            if (item._id == "Namibia") {
                item.name = item._id;
                item._id = "NA";
            }
            if (item._id == "Nueva Caledonia") {
                item.name = item._id;
                item._id = "NC";
            }
            if (item._id == "Níger") {
                item.name = item._id;
                item._id = "NE";
            }
            if (item._id == "Islas Norfolk") {
                item.name = item._id;
                item._id = "NF";
            }
            if (item._id == "Nigeria") {
                item.name = item._id;
                item._id = "NG";
            }
            if (item._id == "Nicaragua") {
                item.name = item._id;
                item._id = "NI";
            }
            if (item._id == "Países Bajos") {
                item.name = item._id;
                item._id = "NL";
            }
            if (item._id == "Noruega") {
                item.name = item._id;
                item._id = "NO";
            }
            if (item._id == "Nepal") {
                item.name = item._id;
                item._id = "NP";
            }
            if (item._id == "Nauru") {
                item.name = item._id;
                item._id = "NR";
            }
            if (item._id == "Zona Neutral") {
                item.name = item._id;
                item._id = "NT";
            }
            if (item._id == "Niue") {
                item.name = item._id;
                item._id = "NU";
            }
            if (item._id == "Nueva Zelanda") {
                item.name = item._id;
                item._id = "NZ";
            }
            if (item._id == "Omán") {
                item.name = item._id;
                item._id = "OM";
            }
            if (item._id == "Panamá") {
                item.name = item._id;
                item._id = "PA";
            }
            if (item._id == "Perú") {
                item.name = item._id;
                item._id = "PE";
            }
            if (item._id == "Polinesia Francesa") {
                item.name = item._id;
                item._id = "PF";
            }
            if (item._id == "Papúa Nueva Guinea") {
                item.name = item._id;
                item._id = "PG";
            }
            if (item._id == "Filipinas") {
                item.name = item._id;
                item._id = "PH";
            }
            if (item._id == "Pakistán") {
                item.name = item._id;
                item._id = "PK";
            }
            if (item._id == "Polonia") {
                item.name = item._id;
                item._id = "PL";
            }
            if (item._id == "San Pedro y Miquelón") {
                item.name = item._id;
                item._id = "PM";
            }
            if (item._id == "Pitcairn") {
                item.name = item._id;
                item._id = "PN";
            }
            if (item._id == "Puerto Rico") {
                item.name = item._id;
                item._id = "PR";
            }
            if (item._id == "Territorios Palestinos") {
                item.name = item._id;
                item._id = "PS";
            }
            if (item._id == "Portugal") {
                item.name = item._id;
                item._id = "PT";
            }
            if (item._id == "Palau") {
                item.name = item._id;
                item._id = "PW";
            }
            if (item._id == "Paraguay") {
                item.name = item._id;
                item._id = "PY";
            }
            if (item._id == "Qatar") {
                item.name = item._id;
                item._id = "QA";
            }
            if (item._id == "Isla Reunión") {
                item.name = item._id;
                item._id = "RE";
            }
            if (item._id == "Rumania") {
                item.name = item._id;
                item._id = "RO";
            }
            if (item._id == "Rusia") {
                item.name = item._id;
                item._id = "RU";
            }
            if (item._id == "Ruanda") {
                item.name = item._id;
                item._id = "RW";
            }
            if (item._id == "Arabia Saudí") {
                item.name = item._id;
                item._id = "SA";
            }
            if (item._id == "Islas Salomón") {
                item.name = item._id;
                item._id = "SB";
            }
            if (item._id == "Islas Seychelles") {
                item.name = item._id;
                item._id = "SC";
            }
            if (item._id == "Sudán") {
                item.name = item._id;
                item._id = "SD";
            }
            if (item._id == "Suecia") {
                item.name = item._id;
                item._id = "SE";
            }
            if (item._id == "Singapur") {
                item.name = item._id;
                item._id = "SG";
            }
            if (item._id == "Santa Helena") {
                item.name = item._id;
                item._id = "SH";
            }
            if (item._id == "Eslovenia") {
                item.name = item._id;
                item._id = "SI";
            }
            if (item._id == "Islas Svalbard y Jan Mayens") {
                item.name = item._id;
                item._id = "SJ";
            }
            if (item._id == "Eslovaquia") {
                item.name = item._id;
                item._id = "SK";
            }
            if (item._id == "Sierra Leona") {
                item.name = item._id;
                item._id = "SL";
            }
            if (item._id == "San Marino") {
                item.name = item._id;
                item._id = "SM";
            }
            if (item._id == "Senegal") {
                item.name = item._id;
                item._id = "SN";
            }
            if (item._id == "Somalia") {
                item.name = item._id;
                item._id = "SO";
            }
            if (item._id == "Surinam") {
                item.name = item._id;
                item._id = "SR";
            }
            if (item._id == "URSS") {
                item.name = item._id;
                item._id = "SU";
            }
            if (item._id == "Santo Tomé y Príncipe") {
                item.name = item._id;
                item._id = "ST";
            }
            if (item._id == "El Salvador") {
                item.name = item._id;
                item._id = "SV";
            }
            if (item._id == "Siria") {
                item.name = item._id;
                item._id = "SY";
            }
            if (item._id == "Suazilandia") {
                item.name = item._id;
                item._id = "SZ";
            }
            if (item._id == "Islas Turks y Caicos") {
                item.name = item._id;
                item._id = "TC";
            }
            if (item._id == "Chad") {
                item.name = item._id;
                item._id = "TD";
            }
            if (item._id == "Tierras Australes y Antárticas Francesas") {
                item.name = item._id;
                item._id = "TF";
            }
            if (item._id == "Togo") {
                item.name = item._id;
                item._id = "TG";
            }
            if (item._id == "Tailandia") {
                item.name = item._id;
                item._id = "TH";
            }
            if (item._id == "Tayikistán") {
                item.name = item._id;
                item._id = "TJ";
            }
            if (item._id == "Tokelau") {
                item.name = item._id;
                item._id = "TK";
            }
            if (item._id == "Turkmenistán") {
                item.name = item._id;
                item._id = "TM";
            }
            if (item._id == "Túnez") {
                item.name = item._id;
                item._id = "TN";
            }
            if (item._id == "Tonga") {
                item.name = item._id;
                item._id = "TO";
            }
            if (item._id == "Timor Oriental") {
                item.name = item._id;
                item._id = "TP";
            }
            if (item._id == "Turquía") {
                item.name = item._id;
                item._id = "TR";
            }
            if (item._id == "Trinidad y Tobago") {
                item.name = item._id;
                item._id = "TT";
            }
            if (item._id == "Tuvalu") {
                item.name = item._id;
                item._id = "TV";
            }
            if (item._id == "Taiwán") {
                item.name = item._id;
                item._id = "TW";
            }
            if (item._id == "Tanzania") {
                item.name = item._id;
                item._id = "TZ";
            }
            if (item._id == "Ucrania") {
                item.name = item._id;
                item._id = "UA";
            }
            if (item._id == "Uganda") {
                item.name = item._id;
                item._id = "UG";
            }
            if (item._id == "Reino Unido") { //Reino Unido -> Inglaterra
                item.name = item._id;
                item._id = "GB";
            }
            if (item._id == "Islas Ultramarinas de Estados Unidos") {
                item.name = item._id;
                item._id = "UM";
            }
            if (item._id == "Estados Unidos de América") {
                item.name = item._id;
                item._id = "US";
            }
            if (item._id == "Uruguay") {
                item.name = item._id;
                item._id = "UY";
            }
            if (item._id == "Uzbekistán") {
                item.name = item._id;
                item._id = "UZ";
            }
            if (item._id == "Vaticano") {
                item.name = item._id;
                item._id = "VA";
            }
            if (item._id == "San Vicente y las Granadinas") {
                item.name = item._id;
                item._id = "VC";
            }
            if (item._id == "Venezuela") {
                item.name = item._id;
                item._id = "VE";
            }
            if (item._id == "Islas Vírgenes Británicas") {
                item.name = item._id;
                item._id = "VG";
            }
            if (item._id == "Islas Vírgenes Americanas") {
                item.name = item._id;
                item._id = "VI";
            }
            if (item._id == "Vietnam") {
                item.name = item._id;
                item._id = "VN";
            }
            if (item._id == "Vanuatu") {
                item.name = item._id;
                item._id = "VU";
            }
            if (item._id == "Islas Wallis y Futuna") {
                item.name = item._id;
                item._id = "WF";
            }
            if (item._id == "Samoa") {
                item.name = item._id;
                item._id = "WS";
            }
            if (item._id == "Yemen") {
                item.name = item._id;
                item._id = "YE";
            }
            if (item._id == "Mayotte") {
                item.name = item._id;
                item._id = "YT";
            }
            if (item._id == "Yugoslavia (antiguo país)") {
                item.name = item._id;
                item._id = "YU";
            }
            if (item._id == "Sudáfrica") {
                item.name = item._id;
                item._id = "ZA";
            }
            if (item._id == "Zambia") {
                item.name = item._id;
                item._id = "ZM";
            }
            if (item._id == "Zaire (antiguo país)") {
                item.name = item._id;
                item._id = "ZR";
            }
            if (item._id == "Zimbabwe") {
                item.name = item._id;
                item._id = "ZW";
            }
            /*if (item._id == "Inglaterra") {
                item.name = item._id;
                item._id = "ENG";
            }*/
            if (item._id == "") {
                item.name = item._id;
                item._id = "";
            }
            return item;
        }
    }

    componentWillUnmount() {
        if (this.chart) {
            this.chart.dispose();
        }

        if (this.intervalID === null) return;
        clearInterval(this.intervalID);
    }

    render() {
        return (
            <div className="mapChart">
                <div className="stats">
                    {/*<div className="">*/}
                    <h6 className="mt-1">Visualizador De Personas Vacunadas En Tiempo Real COVID-19</h6>
                    <Button className="Desintegrer" id="Desintegrer_" variant="contained" color="secondary" >
                        Undo chart
                            <HdrWeak className="HdrWeak" />
                    </Button>
                    {/*</div>*/}
                    <p className="h3 m-0">
                        <span className="mr-xs fw-normal">
                        </span>
                        <i className="fa fa-map-marker" />
                    </p>
                </div>
                <div className="map" id="chartdiv">
                    <span>Alternative content for the map</span>
                </div>
            </div>
        );
    }
}

export default Am4chartMap;

