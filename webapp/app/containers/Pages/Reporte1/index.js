import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { PapperBlock } from 'dan-components';
import RealReporte1 from '../../../components/MyComponents/Reporte1';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import './style.css';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: '1%',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  cardAnimation: {
    'animation': 'appear 500ms ease-out forwards'
  },
}));

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  },
  redBtn: {
    color: '#f44336',
    'border-color': '#f44336'
  },
  blueBtn: {
    color: '#7986cb',
    'border-color': '#7986cb'
  },
  blackBtn: {
    color: '#000000',
    'border-color': '#000000'
  },
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
});

function Reporte1(props) {
  const title = brand.name + ' - Blank Page';
  const description = brand.desc;

  const { classes } = props;
  const classes_ = useStyles();

  return (
    <RealReporte1 />
  );
}
//className="cardAnimation"
Reporte1.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Reporte1);
