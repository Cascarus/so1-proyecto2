# Sistemas Operativos 1

Proyecto 2

## Grupo 21

| Carnet | Integrante |
| :----: | :----: |
| 201700328 | Marcos Alberto Santos Aquino |
| 201314632 | Edgar Orlando Guamuch Zarate |
| 201612204 | Marylin Irenia Hernández Muñoz |
| 201503878 | Erick Fernando Sanchez Mejia |

## Arquitectura

![Arquitectura](./img/img1.png)

## Archivo de entrada

En Locust en el [archivo de entrada](./Kubernetes/traffic.json) hay un array con elementos con la siguiente estructura:

```json
{
    "name":"Jane Doe",
    "location":"Guatemala",
    "gender":"female",
    "age":"27",
    "vaccine_type": "Sputnik V"
}
```

## Locust

Locust es una herramienta de prueba de rendimiento escalable, se utilizara para enviar trafico al ingress, en esta herramienta seran definido el numero de usuarios, el tiempo en el que estos se generan y la direccion del servicio que se utilizara.  
Para correr locust:

```bash
    locust --locustfile traffic.py
```

Se abrira en la pagina por defecto <http://localhost:8089>

## Kubernetes

Para aplicar los deployments y services

```bash
kubectl apply -f archivo.yaml
```

Verificar la arquitectura

```bash
kubectl get deployments -n project
kubectl get services -n project
kubectl get ingress -n project
kubectl get pods -n project
```

### Chaos Mes

Para instalo se ejecuta el comando de la documentacion oficial "https://chaos-mesh.org/docs/user_guides/installation/"

```bash
curl -sSL https://mirrors.chaos-mesh.org/v1.2.0/install.sh | bash
```

Para aplicar un experimento se ejecuta el siguiente comando

```bash
kubectl apply -f experimento.yaml
```

### Linkerd

Se instala siguiendo los pasos de la documentacion oficial "https://linkerd.io/2.10/getting-started/"

#### Ingress

Siguiendo los pasos de la documentacion oficial "https://linkerd.io/2.10/tasks/using-ingress/"

- Se agrega en archivo yaml del ingress lo especficado
- Se inyecta ingress con el comando dado en la documentacion oficial.

kubectl get deployment nginx-ingress-ingress-nginx-controller  -n nginx-ingress -o yaml | linkerd inject --ingress - | kubectl apply -f -

#### Inyectan los deployments

```bash
kubectl get -n project deployments api-grpc-deployment -o yaml \
  | linkerd inject - \
  | kubectl apply -f -
```

```bash
kubectl get -n project deployments api-redis-pub-sub-deployment -o yaml \
  | linkerd inject - \
  | kubectl apply -f -
```

#### Aplicar el faulty traffic

```bash
kubectl apply -f faulty-traffic.yaml
```

## Preguntas

1. Como pueden interpretarse las 4 pruebas utilizando en base a las graficas y metricas que muestra Linkerd y grafana:
En Linkerd se puede tomar la latencia de cada aplicacion de mensajeria y se puede ver que en algunos periodos de tiempo se tiene estabilidad en la latencia y otros donde se vuelve inestable, de igual manera en grafana se puede tomar la grafica de volumenes donde cuando las lineas bajan se interpreta que los datos no estan llegando a alguna aplicacion de mensajeria.

2. ¿Qué patrones de conducta fueron descubiertos?
Sin el experimento el success rate fue del 100%
Con el experimento el success rate fue menor al 100% oscilando entre el rango de 50% a 70%
Cuando se envian datos a alguna aplicacion de mensajeria el reques volumen disminuye

3. ¿Qué sistema de mensajería es más rápido? ¿Por qué?
 El más rápido es Redis pub/sub por su capacidad de manejar hasta 81 mil sets por segundo y 110 mil gets por segundo.
Cual es + rapido = app < latencia

4. ¿Cuántos recursos utiliza cada sistema de mensajería?
PENDIENTE
recursos c/sistema = comando kubernetes de cuanto usa c/deployment

5. ¿Cuáles son las ventajas y desventajas de cada servicio de mensajería?

| Servicio | Ventaja  | Desventaja|
| :----: | :----: |:----:|
| Redis pub sub| -  Permite la carga de imágenes y comentarios en tiempo real sobre esas imágenes. | -Redis es un agente de caché empresarial, es decir debe ser usado en tareas específicas|
| gRPC | -Definición de servicio simple  -Funciona en todos los idiomas y plataformas | -No es comparable a un sistema de colas asincrónico |
| Kafka | -De forma predeterminada, utiliza la persistencia, utiliza la memoria caché del disco del sistema operativo para datos activos (tiene un rendimiento más alto que cualquiera de los anteriores con la persistencia habilit| -Dificultades operativas|



6. ¿Cuál es el mejor sistema de mensajería?
Cada sistema de mensajeria esta diseñado para distintas situaciones, por lo cual no existe una mejor que otra.

mejor sistema = app < latencia & consume < recursos

7. ¿Cuál de las dos bases de datos se desempeña mejor y por qué?
Redis es mejor que MongoDb en las lecturas para todo tipo de cargas de trabajo, tambien es mejor para las escriturar conforme la carga de trabajo vaya incrementando.
Mongo usa todos los núcleos del sistema, Redis logra mas ejecuntandose sobre un solo nucleo sin saturarlos, aunque a su vez consuma mayor memoria RAM que Mongodb para la misma camtidad de almacenamiento


8. ¿Cómo se refleja en los dashboards de Linkerd los experimentos de Chaos Mesh?
Se ven reflejados en la latencia y en el volumen rate ademas del success rate ya que ciertos experimentos dejan abajo un servicio por mucho tiempo y esto hace que el success rate disminuya y se encuentre bastante bajo.

9. ¿En qué se diferencia cada uno de los experimentos realizados?
En que unos afectan los pod como pod failure o pod kill y otros los containers como container kill, afectan diferente parte de la infraestructura del proyecto, tomando de ejemplo pod kill y container kill, con pod kill se mata el pod en el que se estan ejecutando los servicios de toda esa aplicacion de mensajeria, en cambio si se elimina solo un container aun siguen activas el resto de partes de esa la aplicacion de mensajeria que estaba en ese pod, por lo que tarda menos tiempo en restaurar el servicio, caso contrario si se realiza pod kill ya que se esta quitando todo el servicio y se tendra que lenvantar todos los containers de ese pod para brindar el servicio de esa aplicacion de mensajeria.

10. ¿Cuál de todos los experimentos es el más dañino?
Pod kill, con pod kill se mata el pod en el que se estan ejecutando los containers de toda esa aplicacion de mensajeria, si la aplicacion de mensajeria usaba 3 containers todo se ha dado de baja, por lo que tarda mas tiempo en restaurar el servicio, ya que se esta quitando todo el servicio y se tendra que lenvantar todos los containers de ese pod para brindar el servicio de esa aplicacion de mensajeria nuevamente.

¿Cómo funcionan las golden metrics?

Las métricas de oro (o "señales de oro") son las métricas de primera línea para saber si la aplicación está funcionando como se esperaba. Estas métricas brindan una señal de un vistazo sobre el estado de un servicio, sin necesidad de saber qué hace realmente el servicio.

- La latencia que mide que tan lento o rapido es el servicio (tiempo en el que atiende las solicitudes)
- El trafico brinda una descripción general de cuánta demanda se coloca en el servicio, da una idea de lo demandado que esta un servicio (cantidad de solicitudes por segundo)
- Errores que es la cantidad de solicitudes fallidas (se suele combinar con el trafico general para generar una "tasa de exito")
- Saturacion que es una medida de la carga del sistema, en funcion de sus limitaciones principales (memoria, cpu etc.)

Segun Linkerd:
Tasa de éxito, Este es el porcentaje de solicitudes exitosas durante una ventana de tiempo (1 minuto por defecto en Linkerd).
