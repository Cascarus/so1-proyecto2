package main

import (
	
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"bytes"
	"net/http"
	"io/ioutil"

	"github.com/gomodule/redigo/redis"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	grpcapi "github.com/MarcosAlberto21/comunicacionClienteServidorGRPCwhitgolang"
	"encoding/json"
)

type grpcServer struct{}

// Infectado is struc

type Infectado struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	Gender		 string `json:"gender"`
	Age          int    `json:"age"`
	Vaccine_type string `json:"vaccine_type"`
}

func (*grpcServer) GrpcService(ctx context.Context, req *grpcapi.GrpcRequest) (*grpcapi.GrpcResponse, error) {
	fmt.Printf("grpcServer %v\n", req)
	name, _ := os.Hostname()

	input := req.GetInput()
	result := "[way: gRPC]\n " + input + " server host: " + name

	fmt.Println(input)
	


	strjson := input

	var inf Infectado
	strjson = strings.Replace(strjson, "ñ", "n", len(strjson))
	strjson = strings.Replace(strjson, "Ñ", "N", len(strjson))
	log.Println("transaction succesfull")
	json.Unmarshal([]byte(strjson), &inf)
	//mongoInsert(inf.Name, inf.Location, inf.Age, inf.Infected_type, inf.State)
	redisInsert(strjson)
	mongoInsert(strjson)
	res := &grpcapi.GrpcResponse{
		Response: result,
	}

	return res, nil
}

func main() {
	fmt.Println("Starting Server...")
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	hostname := os.Getenv("SVC_HOST_NAME")

	if len(hostname) <= 0 {
		//hostname = "mired" //tomar en cuenta que este cambia cuando se suba a la red
		hostname = "servidor"
		//hostname = "localhost"
	}

	port := os.Getenv("SVC_PORT")

	if len(port) <= 0 {
		port = "7080"
	}

	lis, err := net.Listen("tcp", ":7080")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)
	grpcapi.RegisterGrpcServiceServer(s, &grpcServer{})

	// reflection service on gRPC server.
	reflection.Register(s)

	go func() {
		fmt.Println("Server gRPC running on ", (hostname + ":" + port))
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	fmt.Println("Stopping the server")
	s.Stop()
	fmt.Println("Closing the listener")
	lis.Close()
	fmt.Println("Server Shutdown")

}


func redisInsert(datos string) {

	conn, err := redis.Dial("tcp", "35.223.218.131:6379")
	if err != nil {
		fmt.Println("error de conexión a la base de datos redis", err)
		return
	}
	conn.Send("LPush", "personas", datos)
	rep, err := conn.Do("get", "personas") // Consulta el valor de vaule con la tecla "a" y devuelve
	users, _ := redis.Strings(conn.Do("LRANGE", "personas", 0, -1))
    // Grab one string value and convert it to type byte
	fmt.Println("funciona", rep)
	fmt.Println(users)

}

func mongoInsert(datos string){
	responseBody := bytes.NewBuffer([]byte(datos))

	resp, err := http.Post("http://35.184.21.178:3200/", "application/json", responseBody)

	if err != nil {
		log.Println("An Error Occured %v", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}
	sb := string(body)
	log.Printf(sb)

}