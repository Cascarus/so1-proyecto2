package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"
	"bytes"
	"net/http"
	"io/ioutil"
	"log"
	//"github.com/gomodule/redigo/redis"
	"github.com/go-redis/redis/v8"
)

// Infectado is struc

type Infectado struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	Gender		 string `json:"gender"`
	Age          int    `json:"age"`
	Vaccine_type string `json:"vaccine_type"`
	Way string `json:"way"`
}

func main() {
	// Create a new Redis Client
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "35.223.218.131:6379",  // We connect to host redis, thats what the hostname of the redis service is set to in the docker-compose
		Password: "", // The password IF set in the redis Config file
		DB:       0,
	})
	// Ping the Redis server and check if any errors occured
	err := redisClient.Ping(context.Background()).Err()
	if err != nil {
		// Sleep for 3 seconds and wait for Redis to initialize
		time.Sleep(3 * time.Second)
		err := redisClient.Ping(context.Background()).Err()
		if err != nil {
			panic(err)
		}
	}

	ctx := context.Background()
	// Subscribe to the Topic given
	topic := redisClient.Subscribe(ctx, "new_users")
	// Get the Channel to use
	channel := topic.Channel()
	// Itterate any messages sent on the channel
	for msg := range channel {
		u := &Infectado{}
		// Unmarshal the data into the user
		err := u.UnmarshalBinary([]byte(msg.Payload))
		if err != nil {
			panic(err)
		}
		out, err4 := json.Marshal(u)
		if err4 != nil {
			panic (err4)
		}
		
		//strjson := fmt.Sprintf("%v", out)
     	redisInsert(string(out))
    	mongoInsert(string(out))
		fmt.Println(string(out))
	
	}
}


// MarshalBinary encodes the struct into a binary blob
// Here I cheat and use regular json :)
func (u *Infectado) MarshalBinary() ([]byte, error) {
	return json.Marshal(u)
}

// UnmarshalBinary decodes the struct into a User
func (u *Infectado) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, u); err != nil {
		return err
	}
	return nil
}

func redisInsert(datos string) {

		// Create a new Redis Client
		redisClient := redis.NewClient(&redis.Options{
			Addr:     "35.223.218.131:6379",  // We connect to host redis, thats what the hostname of the redis service is set to in the docker-compose
			Password: "", // The password IF set in the redis Config file
			DB:       0,
		})
		// Ping the Redis server and check if any errors occured
		err := redisClient.Ping(context.Background()).Err()
		if err != nil {
			// Sleep for 3 seconds and wait for Redis to initialize
			time.Sleep(3 * time.Second)
			err := redisClient.Ping(context.Background()).Err()
			if err != nil {
				panic(err)
			}
		}
		ctx := context.Background()
	
		err2 := redisClient.LPush(ctx,"personas", datos)
		if err2 != nil {
			return 
		}
	   fmt.Println(string(datos))

}

func mongoInsert(datos string){
	fmt.Println(datos)
	responseBody := bytes.NewBuffer([]byte(datos))
	//fmt.Println(string(responseBody))

	resp, err := http.Post("http://35.184.21.178:3200/", "application/json", responseBody)

	if err != nil {
		log.Println("An Error Occured %v", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}
	sb := string(body)
	log.Printf(sb)

}